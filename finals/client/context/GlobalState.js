import React, { createContext, useReducer } from "react";
import AppReducer from "./AppReducer";
import axios from "axios";
//initial State
const initialState = {
  transactions: [],
  error: null,
  loading: true,
};

//Create Context

export const GlobalContext = createContext(initialState);

//Provider Components

export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  //actions

  async function getTransactions() {
    try {
      const res = await axios.get("http://192.168.8.100:5000/api/v1/transactions");
      dispatch({
        type: "GET_TRANSACTIONS",
        payload: res.data.data,
      });
    } catch (err) {
      console.log(err);
    }
  }

  async function deleteTransaction(id) {
    try {
      await axios.delete(`http://192.168.8.100:5000/api/v1/transactions/${id}`);
      dispatch({
        type: "DELETE_TRANSACTION",
        payload: id,
      });
    } catch (err) {
      console.log(err);
      /*dispatch({
        type: "TRANSACTION_ERROR",
        payload: err.response.data.error,
      });*/
    }
  }

  async function addTransaction(transaction) {
    const config = { headers: { "Content-Type": "application/json" } };
    try {
      const res = await axios.post(
        "http://192.168.8.100:5000/api/v1/transactions",
        transaction,
        config
      );
      dispatch({
        type: "ADD_TRANSACTION",
        payload: res.data.data,
      });
    } catch (err) {
     console.log(err);
    }
  }

  return (
    <GlobalContext.Provider
      value={{
        transactions: state.transactions,
        error: state.error,
        loading: state.loading,
        getTransactions,
        deleteTransaction,
        addTransaction,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
