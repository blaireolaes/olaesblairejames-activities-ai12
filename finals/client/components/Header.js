import React from "react";
import { View, Text, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    marginTop: 60,
    marginBottom: 20,
  },
  Head: {
    fontSize: 30,
    textAlign: "center",
    fontWeight: "bold",
    color: "#03DAC5",
  },
});
export default function Header() {
  return (
    <View style={styles.container}>
      <Text style={styles.Head}>Expense Tracker</Text>
    </View>
  );
}
