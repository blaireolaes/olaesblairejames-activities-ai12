import React, { useContext } from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  SafeAreaView,
  Pressable
} from "react-native";
import { GlobalContext } from "../context/GlobalState";
import { numberWithCommas } from "../utils/format";



const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    color: "#F32424",
    textAlign: "center",
  },
  text2: {
    fontSize: 20,
    color: "#5FD068",
    textAlign: "center",
  },
  buttext: {
    color: "orange",
    textAlign: "center",
    marginTop: 5,
    fontSize: 15,
  },
  button: {
    backgroundColor: "#000000",
    paddingBottom: 5,
    borderRadius: 20,
    marginLeft: 35,
    marginRight: 35,
    borderColor: "orange",
    borderWidth: 1,
  },
  container: {
    marginTop: 10,
  },
});
export const Transactions = ({ transactions }) => {
  const { deleteTransaction } = useContext(GlobalContext);

  const sign = transactions.amount < 0 ? "-" : "+";

  return (

    <SafeAreaView>
      <View style={styles.container}>
        <Text style={transactions.amount < 0 ? styles.text : styles.text2} >
          {transactions.text} {sign}₱{numberWithCommas(Math.abs(transactions.amount))}
        </Text>
        <Pressable
          style={styles.button}
          onPress={() => deleteTransaction(transactions._id)}
        >
          <Text style={styles.buttext}>Remove</Text>
        </Pressable>
      </View>
    </SafeAreaView>
  );
};
