import React, { useContext, useEffect } from "react";
import { View, Text, StyleSheet, SafeAreaView, ScrollView } from "react-native";
import { GlobalContext } from "../context/GlobalState";
import { Transactions } from "../components/Transactions";
export default function TransactionList() {
  const { transactions, getTransactions } = useContext(GlobalContext);

  useEffect(() => {
    getTransactions();
  }, []);

  const styles = StyleSheet.create({
    text: {
      fontSize: 25,
      fontWeight: "bold",
      color: "#00FFAB",
      paddingLeft: 10,
      paddingRight: 10,
    },
    View: {
      height: 250, //change this for deisgning
      width: 300,

      textAlign: "center",
      backgroundColor: "#121212",
      borderRadius: 20,
      borderColor: "#03DAC5",
      borderWidth: 1,
    },
  });
  return (
    <SafeAreaView>
      <View style={styles.View}>
        <Text style={styles.text}>History</Text>
        <ScrollView>
          {transactions.map((transactions) => (
            <Transactions key={transactions._id} transactions={transactions} />
          ))}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}
