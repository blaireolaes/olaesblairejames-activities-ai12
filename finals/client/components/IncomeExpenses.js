import React, { useContext } from "react";
import { View, Text, StyleSheet } from "react-native";
import { GlobalContext } from "../context/GlobalState";
import { numberWithCommas } from "../utils/format";

const styles = StyleSheet.create({
  container: {
    //    flex: 1,
    backgroundColor: "#121212",
    marginBottom: 10,
    borderRadius: 30,
    borderColor: "#03DAC5",
    borderWidth: 1,
  },
  incomecon: {
    padding: 20,
  },

  text: {
    color: "#03DAC5",
    fontSize: 20,
    fontWeight: "bold",
  },
  incometext: {
    color: "#5FD068",
    fontSize: 20,
  },
  expensete: {
    color: "#F32424",
    fontSize: 20,
  },
});

export default function IncomeExpenses() {
  const { transactions } = useContext(GlobalContext);

  const amounts = transactions.map((transactions) => transactions.amount);

  const income = amounts
    .filter((item) => item > 0)
    .reduce((acc, item) => (acc += item), 0)
    .toFixed(2);

  const expense = (
    amounts.filter((item) => item < 0).reduce((acc, item) => (acc += item), 0) *
    -1
  ).toFixed(2);
  return (
    <View
      style={[
        styles.container,
        {
          flexDirection: "row",
        },
      ]}
    >
      <View style={styles.incomecon}>
        <Text style={styles.text}>Income</Text>
        <Text style={styles.incometext}>₱{numberWithCommas(income)}</Text>
      </View>
      <View style={styles.incomecon}>
        <Text style={styles.text}>Expense</Text>
        <Text style={styles.expensete}>₱{numberWithCommas(expense)}</Text>
      </View>
    </View>
  );
}
