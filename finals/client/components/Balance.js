import React, { useContext } from "react";
import { Text, View, StyleSheet } from "react-native";
import { GlobalContext } from "../context/GlobalState";
import { numberWithCommas } from "../utils/format";

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
    paddingRight: 100,
  },
  balance: { fontSize: 20, color: "#00FFAB" },
  balance2: { fontSize: 20, color: "#FF0800" },

  number: { fontSize: 30, fontWeight: "bold", color: "#14C38E" },
  number2: { fontSize: 30, fontWeight: "bold", color: "#F32424" },
});
export default function Balance() {
  const { transactions } = useContext(GlobalContext);
  const amounts = transactions.map((transactions) => transactions.amount);
  const total = amounts.reduce((acc, item) => (acc += item), 0).toFixed(2);
  return (
    <View style={styles.container}>
      <Text style={total < 0 ? styles.balance2 : styles.balance}>
        Your Balance
      </Text>
      <Text id="balance" style={total < 0 ? styles.number2 : styles.number}>
        ₱{numberWithCommas(total)}
      </Text>
    </View>
  );
}
