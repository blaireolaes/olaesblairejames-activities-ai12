import React, { useState, useContext } from "react";
import {
  Text,
  SafeAreaView,
  TextInput,
  StyleSheet,
  Pressable,
} from "react-native";
import { GlobalContext } from "../context/GlobalState";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#000000",
    borderRadius: 10,
    borderTopColor: "#03DAC5",
    borderTopWidth: 2,
    marginTop: 10,
  },
  head: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#00FFAB",
  },
  input: {
    backgroundColor: "#000000",
    borderRadius: 10,
    borderBottomColor: "orange",
    borderBottomWidth: 1,
    fontSize: 20,
    color: "orange",
    backgroundColor: "#121212",
    textAlign: "center",
    marginTop: 2,
  },
  buttext: {
    color: "#5FD068",
    textAlign: "center",
    fontSize: 20,
  },
  button: {
    backgroundColor: "#000000",
    marginBottom: 30,
    height: 30,
    borderRadius: 20,
    borderColor: "#5FD068",
    borderWidth: 1,
    marginTop: 20,
  },
});

export default function AddTransaction() {
  var [text, setText] = useState("");
  var [amount, setAmount] = useState(0);
  const { addTransaction } = useContext(GlobalContext);
  const onSubmit = (e) => {
    e.preventDefault();
    const newTransaction = {
      id: Math.floor(Math.random() * 100000000),
      text,
      amount: +amount,
    };
    addTransaction(newTransaction);
  };
  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.head}>Add New Transaction</Text>
      <TextInput
        style={styles.input}
        onChangeText={(e) => setText(e)}
        placeholder="Enter Text"
        placeholderTextColor="#14C38E"
      />
      <Text style={styles.head}>
        Amount {"\n"}Positive = INCOME | Negative = EXPENSE
      </Text>
      <TextInput
        style={styles.input}
        keyboardType="numeric"
        onChangeText={(e) => setAmount(e)}
        placeholder="Enter Amount"
        placeholderTextColor="#14C38E"
      />
      <Pressable
        style={styles.button}
        onPress={onSubmit}
        title="Add Transaction"
      >
        <Text style={styles.buttext}>Add Transaction</Text>
      </Pressable>
    </SafeAreaView>
  );
}
