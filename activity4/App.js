import { StyleSheet, Text, View, ImageBackground } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

//Styles
const styles = StyleSheet.create({
  buttons: {
    paddingTop: 100,
  },
  context: {
    marginTop: 30,
    fontSize: 25,
    marginLeft: 10,
    marginRight: 10,
    color: "#E91E63",
    backgroundColor: "rgba(33, 33, 33, 0.9)",
    borderRadius: 15,
  },
  page: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#212121",
    fontStyle:"italic"
  },
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
  },
  back: {
    flex: 1,
    justifyContent: "center",
  },
});
//Home Page
function HomeScreen({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/undraw_Healthy_options_re_lf9l.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Text style={styles.page}>Home</Text>
        <Text style={styles.context}>
          Why i choose IT course ? 
        </Text>
        <Ionic
          style={styles.buttons}
          name="ios-arrow-forward-circle"
          size={90}
          color={"#E91E63"}
          onPress={() => navigation.navigate("R1")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

//1st Page
function Reason1({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/undraw_version_control_re_mg66.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Text style={styles.page}>Reason 1 (To the Future)</Text>
        <Text style={styles.context}>
          People say technology is the future, so I think it's wise to invest my
          time Learning about it.
        </Text>
        <Ionic
          style={styles.buttons}
          name="ios-arrow-forward-circle"
          size={90}
          color={"#E91E63"}
          onPress={() => navigation.navigate("R2")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}
//Second Page
function Reason2({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/undraw_Investing_re_bov7.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Text style={styles.page}>Reason 2 (High Paid)</Text>
        <Text style={styles.context}>
          BSIT graduates have the potential to get high paying jobs, but it
          think it still depends on the field, but still IT is a good choice to
          learn in this Generation
        </Text>
        <Ionic
          style={styles.buttons}
          name="ios-arrow-forward-circle"
          size={90}
          color={"#E91E63"}
          onPress={() => navigation.navigate("R3")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

//Third Page
function Reason3({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/undraw_Pair_programming_re_or4x.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Text style={styles.page}>Reason 3 (In Demand)</Text>
        <Text style={styles.context}>
          IT's are in demand because of how fast Technology is advancing as it
          advance it requires more people that know how to operate about it
        </Text>
        <Ionic
          style={styles.buttons}
          name="ios-arrow-forward-circle"
          size={90}
          color={"#E91E63"}
          onPress={() => navigation.navigate("R4")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

//Fourth Page
function Reason4({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/undraw_futuristic_interface_4q3p.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Text style={styles.page}>Reason 4 (I love Techonology)</Text>
        <Text style={styles.context}>
          Since I was a kid I've been really amazed by Tech stuff like PCs, TVs,
          Internet and other i just love how it works and it really amazed me
        </Text>
        <Ionic
          style={styles.buttons}
          name="ios-arrow-forward-circle"
          size={90}
          color={"#E91E63"}
          onPress={() => navigation.navigate("R5")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

//Fifth Page
function Reason5({ navigation }) {
  return (
    <View style={styles.view}>
      <ImageBackground
        source={require("./background/undraw_Select_option_re_u4qn.png")}
        resizeMode="contain"
        style={styles.back}
      >
        <Text style={styles.page}>Reason 5 (Challenging)</Text>
        <Text style={styles.context}>
          SometimesIi just love being challenged becausee you always learn
          something from it
        </Text>
        <Ionic
          style={styles.buttons}
          name="ios-home-outline"
          size={90}
          color={"#E91E63"}
          onPress={() => navigation.navigate("Olaes Blaire James C.")}
        ></Ionic>
      </ImageBackground>
    </View>
  );
}

const Stack = createNativeStackNavigator();
export default function App({}) {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        
      >
        <Stack.Screen name="Olaes Blaire James C." component={HomeScreen} />
        <Stack.Screen name="R1" component={Reason1} />
        <Stack.Screen name="R2" component={Reason2} />
        <Stack.Screen name="R3" component={Reason3} />
        <Stack.Screen name="R4" component={Reason4} />
        <Stack.Screen name="R5" component={Reason5} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
