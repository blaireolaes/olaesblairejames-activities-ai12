import React from "react";
import { StatusBar } from "react-native";
import Ionic from "react-native-vector-icons/Ionicons";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import HomeScreen from "./screens/HomeScreen";
import Activity2 from "./screens/Activity2";
import ProfileScreen from "./screens/ProfileScreen";

const App = () => {
  const Tab = createBottomTabNavigator();
  return (
    <>
      <StatusBar backgroundColor="#A1887F" barStyle="dark-content" />
      <NavigationContainer>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, size, colour }) => {
              let iconName;
              if (route.name === "Home") {
                iconName = focused ? "ios-home" : "ios-home-outline";
                size = focused ? size + 8 : size + 5;
              } else if (route.name === "Todolist") {
                iconName = focused ? "list-circle" : "list-circle-outline";
                size = focused ? size + 8 : size + 5;
              } else if (route.name === "Completed Tasks") {
                iconName = focused ? "list-sharp" : "list-outline";
                size = focused ? size + 8 : size + 5;
              }
              return <Ionic name={iconName} size={size} colour={colour} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: "black",
            inactiveTintColor: "black",
            showLabel: false,
            tabStyle: {
              backgroundColor: "#A1887F",
              height: 60,
            },
          }}
        >
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="Todolist" component={Activity2} />
          <Tab.Screen name="Completed Tasks" component={ProfileScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    </>
  );
};
export default App;
