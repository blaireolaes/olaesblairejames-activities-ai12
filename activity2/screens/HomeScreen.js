import { View, Text } from "react-native";
import React from "react";

const HomeScreen = () => {
  return (
    <View
      style={{
        height: "100%",
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Text style={{ fontSize: 40, fontWeight: "bold", letterSpacing: 5 ,color: '#3E2723'}}>
        Olaes Blaire James C. (BSIT AI12)
      </Text>
    </View>
  );
};

export default HomeScreen;
