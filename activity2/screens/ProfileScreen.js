import { View, Text } from "react-native";
import React from "react";

const ProfileScreen = () => {
  return (
    <View
      style={{
        height: "100%",
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Text style={{ fontSize: 40, fontWeight: "bold", letterSpacing: 4 ,color: "#3E2723"}}>
      Completed Tasks Go Here
      </Text>
    </View>
  );
};

export default ProfileScreen;
