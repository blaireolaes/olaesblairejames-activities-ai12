import * as React from "react";
import Button from "./Button";
import { View, Text, ColorPropType } from "react-native";
import { Styles } from "../styles/GlobalStyles";
import { myColors } from "../styles/Colors";
import { createContext } from "react";
export default function MyKeyboard() {
  var datas = new Array("");
  var [transfer, transfer2] = React.useState("");
  var [inputs, setinputs] = React.useState("");
  var [mp, mp2] = React.useState("");
  var [mm, mm2] = React.useState("");
  var [mr, mr2] = React.useState("");
  const [pers, setpers] = React.useState("");
  let [stat, stat2] = React.useState(false);
  var [taskItems, setTaskItems] = React.useState([""]);

  const equal = () => {
    if (stat === false) {
      transfer2((transfer = eval(inputs)));
      history();
    } else {
      pereq();
    }
  };

  const history = () => {
    setTaskItems([...taskItems, inputs + " = " + transfer + " | "]);
  };

  const pereq = () => {
    var eq = "/100*";
    var convert = pers + eq + inputs;
    transfer2((transfer = eval(convert)));
    setTaskItems([
      ...taskItems,
      inputs + " % " + pers + " = " + transfer + " | ",
    ]);
  };
  const per = () => {
    setpers(inputs);
    setinputs(" ");
    stat2(true);
  };

  const mplus = () => {
    mp2((mp = eval(inputs)));
    mr2(eval(mr + mp));
    clear();
  };

  const mminus = () => {
    mm2((mm = eval(inputs)));
    var resut = parseFloat(mr) - parseFloat(mm);
    mr2(resut.toString());
    clear();
  };

  const mrecall = () => {
    setinputs(mr);
    mclear();
  };
  const mclear = () => {
    mp2("");
    mm2("");
    mr2("");
  };

  const handleNumberPressNew = (buttonValue: string) => {
    var val = datas.push(buttonValue);
    setinputs(inputs + datas.join(""));
  };

  const clear = () => {
    mp2("");
    mm2("");
    setinputs("");
    transfer2("");
    setpers("");
    stat2(false);
  };

  const firstNumberDisplay = () => {
    if (parseFloat(transfer) !== 0) {
      return (
        <Text
          style={
            parseFloat(transfer) < 99999
              ? [Styles.screenFirstNumber, { color: myColors.result }]
              : [
                  Styles.screenFirstNumber,
                  { fontSize: 50, color: myColors.result },
                ]
          }
        >
          {transfer?.toString()}
        </Text>
      );
    }
    if (inputs && inputs.length < 6) {
      return <Text style={Styles.screenFirstNumber}>{inputs}</Text>;
    }
    if (inputs === "") {
      return <Text style={Styles.screenFirstNumber}>{"0"}</Text>;
    }
    if (inputs.length > 5 && inputs.length < 8) {
      return (
        <Text style={[Styles.screenFirstNumber, { fontSize: 70 }]}>
          {inputs}
        </Text>
      );
    }
    if (inputs.length > 7) {
      return (
        <Text style={[Styles.screenFirstNumber, { fontSize: 50 }]}>
          {inputs}
        </Text>
      );
    }
  };

  return (
    <View style={Styles.viewBottom}>
      <Text style={{ color: "orange", fontSize: 10, marginRight: 200 }}>
        History
      </Text>
      <Text style={{ color: "#46d0dd", fontSize: 10, marginRight: 200 }}>
        {taskItems}
      </Text>
      <View
        style={{
          height: 120,
          width: "90%",
          justifyContent: "flex-end",
          alignSelf: "center",
        }}
      >
        <Text style={Styles.screenSecondNumber}>
          {inputs}
          <Text style={{ color: "orange", fontSize: 50, fontWeight: "500" }}>
            {pers}
          </Text>
        </Text>

        {firstNumberDisplay()}
      </View>
      <View style={Styles.row}>
        <Button title="C" isGray onPress={clear} />
        <Button title="(" isGray onPress={() => handleNumberPressNew("(")} />
        <Button title=")" isGray onPress={() => handleNumberPressNew(")")} />
        <Button title="%" isGray onPress={() => per()} />
        <Button title="÷" isGray onPress={() => handleNumberPressNew("/")} />
      </View>
      <View style={Styles.row}>
        <Button title="7" onPress={() => handleNumberPressNew("7")} />
        <Button title="8" onPress={() => handleNumberPressNew("8")} />
        <Button title="9" onPress={() => handleNumberPressNew("9")} />
        <Button title="M+" isBlue onPress={() => mplus()} />
        <Button title="+" isBlue onPress={() => handleNumberPressNew("+")} />
      </View>
      <View style={Styles.row}>
        <Button title="4" onPress={() => handleNumberPressNew("4")} />
        <Button title="5" onPress={() => handleNumberPressNew("5")} />
        <Button title="6" onPress={() => handleNumberPressNew("6")} />
        <Button title="M-" isBlue onPress={() => mminus()} />
        <Button title="-" isBlue onPress={() => handleNumberPressNew("-")} />
      </View>
      <View style={Styles.row}>
        <Button title="1" onPress={() => handleNumberPressNew("1")} />
        <Button title="2" onPress={() => handleNumberPressNew("2")} />
        <Button title="3" onPress={() => handleNumberPressNew("3")} />
        <Button title="MR" isBlue onPress={() => mrecall()} />
        <Button title="x" isBlue onPress={() => handleNumberPressNew("*")} />
      </View>
      <View style={Styles.row}>
        <Button title="." onPress={() => handleNumberPressNew(".")} />
        <Button title="0" onPress={() => handleNumberPressNew("0")} />
        <Button title="⌫" onPress={() => setinputs(inputs.slice(0, -1))} />
        <Button title="MC" isBlue onPress={() => mclear()} />
        <Button title="=" isBlue onPress={() => equal()} />
      </View>
    </View>
  );
}
