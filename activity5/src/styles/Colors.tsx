export const myColors = {
    light: '#ffffff',
    dark: '#1e1e1e',
    blue: '#46d0dd',
    btnGray: '#46d0dd',
    btnDark: '#1d2436',
    gray: '#46D5B2',
    black: '#2a2828',
    white: '#ffffff', 
    result: 'orange',
}